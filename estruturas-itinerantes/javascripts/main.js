'use strict'

var app = angular.module('app', [])
			.config(function($routeProvider){
				$routeProvider
					.when('/', {templateUrl: 'under-construction.html'})
					.when('/home', {templateUrl: 'home.html'})
					.when('/a-casa-do-futuro', {templateUrl: 'a-casa-do-futuro.html'})
					.when('/nossos-produtos', {templateUrl: 'nossos-produtos.html'})
					.when('/a-designer', {templateUrl: 'a-designer.html'})
					.when('/nossos-parceiros', {templateUrl: 'nossos-parceiros.html'})
					.when('/contato', {templateUrl: 'contato.html'})
			})