(function () {
	'use strict'

	function openContent (e) {
		e.preventDefault();

		var el = $(this),
			selected = el.data('selected')

		$('.vertical-nav a').removeClass('active');
		el.addClass('active');

		$('.content-desc').hide().eq(selected).show();
	}

	$(function () {
		$('.vertical-nav ul li a').on('click', openContent);
	});
}());