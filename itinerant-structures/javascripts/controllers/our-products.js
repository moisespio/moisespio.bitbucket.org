(function () {
	'use strict'

	function openProduct (e) {
		e.preventDefault();

		var el = $(this),
			floorplan = el.data('floorplan'),
			image = el.data('image'),
			modal = $('.product-modal')

		modal.find('.floorplan').append('<img src="' + floorplan + '">');
		modal.find('.image').append('<img src="' + image + '">');

		modal.show();
	}

	function closeProduct (e) {
		e.preventDefault();

		$('.product-modal').hide().find('img').remove();
	}

	$(function () {
		$('.close').on('click', closeProduct);
		$('.our-products .catalogue .content ul li a').on('click', openProduct);
	});
}());