'use strict'

var app = angular.module('app', [])
			.config(function($routeProvider){
				$routeProvider
					.when('/', {templateUrl: 'under-construction.html'})
					.when('/home', {templateUrl: 'home.html'})
					.when('/the-house-of-the-future', {templateUrl: 'the-house-of-the-future.html'})
					.when('/our-products', {templateUrl: 'our-products.html'})
					.when('/the-designer', {templateUrl: 'the-designer.html'})
					.when('/media', {templateUrl: 'media.html'})
					.when('/contact', {templateUrl: 'contact.html'})
			})